# Bot Owners

These OWNERS files should be named for the resource we want to grant access to.
A specific bot is expected to be listed in multiple OWNERS files here rather
than a specific OWNERS file reused for an unrelated resource.
This helps prevent inadvertently granting permissions to bots that may get
added, allows changing the bots being used for specific tasks at a more
granular level, and follows Ganpati best practices by assigning roles to policy
groups to manage resources.
