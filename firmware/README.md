# Firmware OWNERS

This directory contains OWNERS lists for various firmware-related teams. To
simplify organization, each file generally corresponds directly to a Google
Group for the respective team. Since directly linking to a Group is not
possible, we have to manually copy member names out of
<https://expn.corp.google.com/> and manually update the lists as needed when
memberships change.

Note that hyphens (`-`) seem to be illegal in OWNERS file names for some strange
reason, so they have to be replaced with underscores (`_`) in the filenames
(e.g. the OWNERS file for `cros-ap@google.com` is called `OWNERS.cros_ap`).
