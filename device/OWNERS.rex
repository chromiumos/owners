# Rex baseboard team

andrescj@google.com
gwendal@google.com
hiroh@google.com
htcheong@google.com
jcliang@google.com
jimmysun@chromium.org
kamesan@google.com
kapilporwal@google.com
lalithkraj@google.com
levinale@google.com
mcchou@google.com
mparuchuri@google.com
pmalani@google.com
rajatja@google.com
scollyer@google.com
senozhatsky@chromium.org
subratabanik@google.com
yueherngl@google.com
zhuohao@google.com

# Allow SIE maintenance
include /sie/OWNERS.chromeos_sie
