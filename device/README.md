# "Device" OWNERS

This grouping of OWNERS is intended to roughly correspond with the devices
which we support.  Early in design these OWNERS will likely be bringup teams.
Later they will transition to SIE's.
