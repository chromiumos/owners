# "Apps" OWNERS

This grouping of OWNERS is intended to roughly correspond with the "Apps"
Superpillar (go/cros-apps) in the CrOS Software organization.
