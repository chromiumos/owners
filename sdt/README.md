# ChromeOS System Design Team

ChromeOS System Design Team (SDT) will develop and implement simpler,
standard ChromeOS development processes that are efficient and
effective for ChromeOS Partners.

## Team Responsibilities

### ChromeOS Platform automation

Utilizing DLM platform workflows and automation, makes ChromeOS Google
teams and Partners life easier and allows them focus on critical tasks.

### ODM Enablement and Training

Design and provide an efficient training program and on-boarding
processes for new and existing partners such as OEM, ODMs, LODMs,
Vendors, etc...

### ChromeOS Identification and Configurations

Design an identification and configuration supporting system based on a
single source of truth to serve ChromeOS ecosystem usage.
